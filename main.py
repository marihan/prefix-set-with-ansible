import subprocess, json


class PrefixSet:
  def __init__(self, prefix_set_name ,prefix_set_values):
    self.name = prefix_set_name
    self.values=prefix_set_values


def run_playbook(playbookLi):
    result = subprocess.Popen(playbookLi, stdout=subprocess.PIPE)
    output, err = result.communicate()
    return output,err


def Convert(string):
    li = list(string.split("  "))
    return li

def get_prefix_set(prefixSetName):
    playbookCmd=['ansible-playbook', 'get-prefix-set.yml', '-i', 'hosts','--extra-vars' ,'prefixSet='+prefixSetName]
    output,err=run_playbook(playbookCmd)
    file=open("exist-prefix-set" , "r")
    f=file.read()
    f=f.replace('\\n','').replace(',','').replace('end-set!"]','')

    # print(f)
    li_prefix_set=Convert(f)
    values={}
    key=""
    for i in li_prefix_set:
        if(i.startswith('#')):
            values[i]=[]
            key=i
        elif(i.startswith('1')):
            values[key].append(i)
    name=str(li_prefix_set[0].split(" ")[1])
    prefix_set=PrefixSet(name,values)
    return prefix_set


def push_prefix_set(prefixSet):
    the_file=open("iosxr_prefix_set.cfg" , "w")
    the_file.seek(0)
    the_file.write('prefix-set '+ prefixSet.name+ '\n')
    for comment in prefixSet.values:
        the_file.write(comment+ '\n')
        for ip in prefixSet.values[comment]:
            if(ip == prefix_set.values[prefix_set.values.keys()[-1]][-1]):
                the_file.write(ip+ '\n')
            else:
                the_file.write(ip+ ',\n')
    the_file.write('end-set\n!')
    playbookCmd=['ansible-playbook', 'set-prefix-set.yml', '-i', 'hosts']
    output,err=run_playbook(playbookCmd)
    # print(output)

def append_prefix_set(prefixSet , comment , ips):
    prefixSet.values[comment]=ips
    return prefixSet


def get_prefix_set_names():
    file=open("all-prefix-set" , "r")
    f=file.read()
    f=f.replace('end-set','').replace('!','').replace('"]','')
    f=f.replace('\\n','').replace(',','').replace('["Listing for all Prefix Set objects','')
    f=f.replace('prefix-set ','  prefix-set ') #take care of spaces, i remove space after and add one before
    all_prefix_set=Convert(f)
    Li_all_prefix_set=[]
    values={}
    key=""
    name=""
    for i in all_prefix_set:
        print(i)
        if(i.startswith('p')):
            name=str(i.split(" ")[1])
            Li_all_prefix_set.append(name)
    print(Li_all_prefix_set)



get__all_prefix_set()
# prefix_set_name="SWH3"
# prefix_comment="# EnodeB-S1-SADAT-test"
# prefix_ips=['10.77.190.0/24','10.77.191.0/24']
# prefix_set=get_prefix_set(prefix_set_name)
# # print(prefix_set.values)
# prefix_set=append_prefix_set(prefix_set , prefix_comment , prefix_ips)
# # print(prefix_set.values)
# push_prefix_set(prefix_set)